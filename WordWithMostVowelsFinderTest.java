package com.example.vowelstdd;

import static com.example.vowelstdd.WordWithMostVowelsFinder.findWordWithMostVowels;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class WordWithMostVowelsFinderTest
{
    @Test
    public void emptyList_hasNoWordWithMostVowels()
    {
        assertThat(findWordWithMostVowels(""), equalTo(""));
    }

    @Test
    public void oneWord_hasTheMostVowels()
    {
        assertThat(findWordWithMostVowels("gurlp"), equalTo("gurlp"));
    }

    @Test
    public void twoWords_oneWithMostVowelsIsResult()
    {
        assertThat(findWordWithMostVowels("oook,ook"), equalTo("oook"));
        assertThat(findWordWithMostVowels("okkkk,ook"), equalTo("ook"));
        assertThat(findWordWithMostVowels("a,ook"), equalTo("ook"));
        assertThat(findWordWithMostVowels("fOO,bar,qUeuE,spools"), equalTo("qUeuE"));
    }

    @Test
    public void isVowel_recognizesAllEnglishVowels()
    {
        for (char vowel : "aeuioyAEIOUY".toCharArray())
            assertTrue(vowel + " was not recognized as a vowel.", WordWithMostVowelsFinder.isVowel(vowel));
    }
}
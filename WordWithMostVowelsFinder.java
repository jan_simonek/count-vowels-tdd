package com.example.vowelstdd;

import java.nio.CharBuffer;

public class WordWithMostVowelsFinder
{

    public static String findWordWithMostVowels(String s)
    {
        String wordWithMostVowels = "";
        int maxVowelCount = 0;

        for (String word : splitCommaSeparatedStringToWords(s))
        {
            if (getVowelCount(word) > maxVowelCount)
            {
                maxVowelCount = getVowelCount(word);
                wordWithMostVowels = word;
            }
        }

        return wordWithMostVowels;
    }

    private static int getVowelCount(String word)
    {
        int vowelCount = 0;

        for (char c : word.toCharArray())
            if (isVowel(c))
                vowelCount++;

        return vowelCount;
    }

    public static boolean isVowel(char c)
    {
        return "aeiouyAEIOUY".contains(createCharBufferFromChar(c));
    }

    private static String[] splitCommaSeparatedStringToWords(String commaSeparatedString)
    {
        return commaSeparatedString.split(",");
    }

    private static CharBuffer createCharBufferFromChar(char c)
    {
        CharBuffer charBuffer = CharBuffer.allocate(1);
        charBuffer.append(c);
        charBuffer.position(0);
        return charBuffer;
    }


}
